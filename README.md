# Description

    This project contains two coding exercises described below.

# Exercise 1: Write some code, that will flatten an array of arbitrarily nested arrays of integers into a flat array of integers

In this exercise, implement the flatten function (code/doc/tests). The function is to be used for a higher level of implementation. The module test-all will be used as your main to run, test and output.

Usage examples are provided in the test-all file.

# Exercise 2: Write Tracker class models

In this exercise, you need to implement class models. We want to have a Generic tracker class and a Temperature tracker which will inherit from the Generic.

Generic tracker will need to have a 'insert' and 'mean' function to be used by any type of tracker.
Temperature tracker will have its own specific function such as "min" and "max".

In the test-all file, simply use your trackers.
