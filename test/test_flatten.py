"""
    Description: unit tests for module flatten
"""
from modules.flatten import *


def test_flatten_list():
    assert len(flatten_list([['1', 2, [3]], 4])) == 4
    assert flatten_list([['1', 2, [3]], 4]) == ['1', 2, 3, 4]
    assert len(flatten_list(['1', 2, 3, [4], [], [[[[[[[[[5]]]]]]]]]])) == 5
    assert flatten_list(['1', 2, 3, [4], [], [[[[[[[[[5]]]]]]]]]]) == ['1', 2, 3, 4, 5]
    assert len(flatten_list([])) == 0
