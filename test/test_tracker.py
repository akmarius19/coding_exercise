"""
    Description: unit tests for module tracker
"""
from modules.tracker import *
import pytest


@pytest.fixture()
def tracker():
    return GenTracker()


@pytest.fixture()
def temp_tracker():
    tracker = TempTracker()
    return tracker


def test_insert_invalid_data(tracker):
    assert tracker.insert_values([]) is None


def test_insert_valid_data(temp_tracker):
    temp_tracker.insert_values([22, 7, 22, 13, 2, 31, 18])
    assert len(temp_tracker.insert_values([25, 48, 0])) == 10


def test_mean_value(temp_tracker):
    assert temp_tracker.get_mean() == 18.8


def test_min_value(temp_tracker):
    assert temp_tracker.get_min() == 0


def test_max_value(temp_tracker):
    assert temp_tracker.get_max() == 48
