#!/usr/bin/python
"""test-all.py - main testing file
"""
from modules.flatten import *
from modules.tracker import *


def run_flatten():
    print(flatten_list([['1', 2, [3]], 4]))
    print(flatten_list(['1', 2, 3, [4], [], [[[[[[[[[5]]]]]]]]]]))


def run_trackers():
    temperatures = [22, 7, 22, 13, 2, 31, 18]
    tracker = TempTracker()
    tracker.insert_values(temperatures)
    print(tracker.get_max())
    print(tracker.get_min())
    print(tracker.get_mean())


if __name__ == '__main__':
    run_flatten()
    run_trackers()
