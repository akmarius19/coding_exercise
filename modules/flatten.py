#!/usr/bin/python
"""
    flatten.py - extended utils
"""


def flatten_list(nested_list, array_types=(list, tuple)):
    try:
        for i, j in enumerate(nested_list):
            while i < len(nested_list) and isinstance(nested_list[i], array_types):
                nested_list[i:i + 1] = nested_list[i]
        return nested_list
    except BaseException as e:
        print('Something went wrong ! %s error occurred', e)
