#!/usr/bin/python
"""
    tracker.py - extended utils
"""


class GenTracker:
    data = []
    average_temp = 0

    def insert_values(self, data: list):
        if not isinstance(data, list) or len(data) == 0:
            print('Please provide a list of values to be inserted')
            return
        for item in data:
            self.data.append(item)
        print('Insertion complete')
        return self.data

    def get_mean(self):
        self.verify_data()
        self.average_temp = float('{:.2f}'.format(sum(self.data) / len(self.data)))
        return self.average_temp

    def verify_data(self):
        if len(self.data) == 0:
            print('Please insert some values before data processing...')
            exit()
        for item in self.data:
            if not str(item).isdigit():
                print('Only numeric values allowed !')
                exit()


class TempTracker(GenTracker):
    min_temp = 0
    max_temp = 0

    def get_min(self):
        self.verify_data()
        self.min_temp = min(self.data)
        return self.min_temp

    def get_max(self):
        self.verify_data()
        self.max_temp = max(self.data)
        return self.max_temp
